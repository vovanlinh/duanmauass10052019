package com.example.bookmanager1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.bookmanager1.DAO.ThemNguoiDungDao;
import com.example.bookmanager1.NguoiDung.Dangkytk;
import com.example.bookmanager1.model.Nguoidung;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.ArrayList;
import java.util.Map;

public class login extends AppCompatActivity {
    EditText edtusername,edtpass;
    TextInputLayout inputtk,inputmk;
    Button btndangnhap,btndangky;
    CheckBox cbluu;
    SharedPreferences luutru;
    //nguoi dung
    ArrayList<Nguoidung> list;

    DatabaseReference mDatabase;
    static ThemNguoiDungDao dao;
    String us;
    String ps;
    ArrayList<Nguoidung> userlist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        anhxa();
//login
dao=new ThemNguoiDungDao(this);
        //
        luutru = getSharedPreferences("loginthuchi", Context.MODE_PRIVATE);

        // nap thong tin len form tu sharedpreference
        Boolean luuthongtin = luutru.getBoolean("saveInformation", false);
        if (luuthongtin) {
            edtusername.setText(luutru.getString("username_info", ""));
            edtpass.setText(luutru.getString("password_info", ""));
            cbluu.setChecked(true);

        }

        mDatabase= FirebaseDatabase.getInstance().getReference().child("Nguoidung");

        final String username = edtusername.getText().toString();
        final String pass = edtpass.getText().toString();

        btndangnhap.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {


                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds:dataSnapshot.getChildren()){
                            Map<String,Object> map=(Map<String, Object>)ds.getValue();
                            Object price=map.get("usernd");
                            Object sl=map.get("passnd");
                            String pvalue=String.valueOf(price);
                            String  slvalue=String.valueOf(sl);
                            if (edtusername.getText().toString().isEmpty()){
                                inputtk.setError("User khong dc de trong");
                                return;
                            }
                          else if (edtpass.getText().toString().isEmpty()){
                                inputtk.setError("pass khong dc de trong");
                                return;
                            }
                           else if ( edtusername.getText().toString().equals(pvalue)&&edtpass.getText().toString().equals(slvalue)){
                                Toast.makeText(getApplicationContext(), "Login thanh cong", Toast.LENGTH_SHORT).show();

                                Intent intent=new Intent(login.this,MainActivity.class);
                                startActivity(intent);
break;

                            }
                           else if ((!edtusername.getText().toString().equals(pvalue))){
                                inputtk.setError("Sai thong tin tk");

                                }
                           else   if ((!edtpass.getText().toString().equals(slvalue))){
                              //  inputtk.setError("");
                                inputmk.setError("Sai thong tin mk");

                            }
                        }


                    }



                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                SharedPreferences.Editor editor = luutru.edit();

                if (cbluu.isChecked()) {
                    editor.putString("username_info", edtusername.getText().toString());
                    editor.putString("password_info", edtpass.getText().toString());
                }
                editor.putBoolean("saveInformation", cbluu.isChecked());
                editor.commit();





                /*
                if ( edtusername.getText().toString().equals("123")&&edtpass.getText().toString().equals("123")){
                    Toast.makeText(getApplicationContext(), "Login thanh cong", Toast.LENGTH_SHORT).show();

                    SharedPreferences.Editor editor = luutru.edit();
                    if (cbluu.isChecked()) {
                        editor.putString("username_info", edtusername.getText().toString());
                        editor.putString("password_info", edtpass.getText().toString());
                    }
                    editor.putBoolean("saveInformation", cbluu.isChecked());
                    editor.commit();


                    Intent intent=new Intent(login.this,MainActivity.class);
                    startActivity(intent);

                 */

            }

        });
        btndangky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(login.this, Dangkytk.class);
                startActivity(intent);
            }
        });

    }



    private void anhxa() {
        edtusername=findViewById(R.id.edtusername);
        edtpass=findViewById(R.id.edtpass);
        btndangnhap=findViewById(R.id.btndangnhap);
        cbluu=findViewById(R.id.cbluu);
        inputtk=findViewById(R.id.inputtk);
        inputmk=findViewById(R.id.inputmk);
        btndangky=findViewById(R.id.btndangky);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
