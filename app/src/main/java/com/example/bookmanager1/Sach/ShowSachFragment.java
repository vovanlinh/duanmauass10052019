package com.example.bookmanager1.Sach;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bookmanager1.Adapter.SachAdaptercv;
import com.example.bookmanager1.DAO.ThemSachDao;
import com.example.bookmanager1.DAO.ThemTheLoaiDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.Sach;

import java.util.ArrayList;
import java.util.List;

public class ShowSachFragment extends Fragment {

    RecyclerView lv;
    /*
    ArrayList<Sach> list;

    static ThemSachDao dao;

     */

    Spinner spnbook;
    ThemTheLoaiDao dao;
    ArrayList<String> list;
    static ArrayAdapter<String>adapter;
    public static SachAdaptercv bookadapter;
    static ThemSachDao sdao;
    List<Sach>booklist;
    String character;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_showsach,container,false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spnbook=view.findViewById(R.id.spntentheloai);
        lv = view.findViewById(R.id.lv_showsach);

sdao=new ThemSachDao(getContext());
list=new ArrayList<>();
dao=new ThemTheLoaiDao(getContext());
list=dao.getidbook();
adapter=new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,list);
spnbook.setAdapter(adapter);

booklist=new ArrayList<Sach>();
booklist=sdao.getIdbook(character);
bookadapter=new SachAdaptercv(getContext(), (ArrayList<Sach>) booklist);
        LinearLayoutManager mlinerlayoutmanager=new LinearLayoutManager(getContext());
        lv.setLayoutManager(mlinerlayoutmanager);
lv.setAdapter(bookadapter);


spnbook.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
Object item =spnbook.getSelectedItem().toString();
String[] chacraccterid=((String) item).split("-");
character=chacraccterid[0];
sdao=new ThemSachDao(getContext());
booklist=sdao.getIdbook(character);
bookadapter=new SachAdaptercv(getContext(), (ArrayList<Sach>) booklist);
LinearLayoutManager mlinerlayoutmanager=new LinearLayoutManager(getContext());
        lv.setLayoutManager(mlinerlayoutmanager);
        lv.setAdapter(bookadapter);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
});
        EditText edtserch=view.findViewById(R.id.edttimmkiem);
        edtserch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count < before) {
                    bookadapter.resetData();
                }
                bookadapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
    public static void xoaSach(Sach s){

        sdao.delete(s);
        capnhatLV();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public static void capnhatLV(){

        bookadapter.notifyDataSetChanged();
    }
    public static void capnhatLVTL(){

        adapter.notifyDataSetChanged();
    }
}
