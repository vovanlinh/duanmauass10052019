package com.example.bookmanager1.Sach;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.bookmanager1.Adapter.spinTenTheLoaiSach;
import com.example.bookmanager1.DAO.ThemSachDao;
import com.example.bookmanager1.DAO.ThemTheLoaiDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.Sach;
import com.example.bookmanager1.model.Theloai;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

public class ThemSachFragment extends Fragment {
Spinner spntentheloai;
EditText masach,tensach,nhanxb,tacgia,soluong,giabia,mota;
    Button btnthem,btnhuy,btnshow;
    Sach nd;
    ThemSachDao dao;
    //spin
    ThemTheLoaiDao ldao;
    ArrayList<Theloai> danhsachtenlt;
   static spinTenTheLoaiSach spintenloaimaloai;
    String matheloaisach="";
    TextInputLayout inputms,inputtensach,inputtacgia,inputgiabia,inputsoluong,inputnxb,inputmota;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_themsach,container,false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//anhxa
        masach=view.findViewById(R.id.edtmasach);
        tensach=view.findViewById(R.id.edttensach);
        nhanxb=view.findViewById(R.id.edtnxb);
        tacgia=view.findViewById(R.id.edttentacgia);
        giabia=view.findViewById(R.id.edtgiabia);
        soluong=view.findViewById(R.id.edtsoluong);
        btnthem=view.findViewById(R.id.btnthemsach);
        btnhuy=view.findViewById(R.id.btnhuysach);
       // inputms=view.findViewById(R.id.inputmasachthem);
       // inputtensach=view.findViewById(R.id.inputtensachthem);
       // inputgiabia=view.findViewById(R.id.inputgiabiathem);
      //  inputnxb=view.findViewById(R.id.inputnxbthem);
      //  inputsoluong=view.findViewById(R.id.inputsoluongthem);

       // inputtacgia=view.findViewById(R.id.inputtaccgiathem);
mota=view.findViewById(R.id.edtmota);
        spntentheloai=view.findViewById(R.id.spntheloai);
        //----------------
        ldao=new ThemTheLoaiDao(getContext());
        danhsachtenlt=new ArrayList<>();
        danhsachtenlt= (ArrayList<Theloai>) ldao.getTenTheLoai();
        spintenloaimaloai=new spinTenTheLoaiSach(getContext(),danhsachtenlt);
        spntentheloai.setAdapter(spintenloaimaloai);
/*
 spntentheloai.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
         @Override
         public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
          matheloaisach=danhsachtenlt.get(spntentheloai.getSelectedItemPosition()).getMaTL()+"";
             //Log.d("textttt",matheloaisach);
         }
         @Override
         public void onNothingSelected(AdapterView<?> parent) {

         }
     });
*/

        dao = new ThemSachDao(getContext());

        btnthem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String masach1 = masach.getText().toString();
                String tensach1 = tensach.getText().toString();
                String nxb1 = nhanxb.getText().toString();
                String tacgia1 = tacgia.getText().toString();
                matheloaisach=danhsachtenlt.get(spntentheloai.getSelectedItemPosition()).getMaTL();
                String giabia1 = giabia.getText().toString();
                String soluong1 = soluong.getText().toString();
                String mota1=mota.getText().toString();
//maSach,maTL,tieude,tacgia,giabia,soluong
         /*
                if (masach.getText().toString().isEmpty()){
                    inputms.setError("ma sach khong dc de trong");
                    return;
                }
              else if (tensach.getText().toString().isEmpty()){
                    inputtensach.setError("ten sach khong dc de trong");
                    return;
                }
              else if (nhanxb.getText().toString().isEmpty()){
                    inputnxb.setError("nha xb khong dc de trong");
                    return;
                }
               else if (tacgia.getText().toString().isEmpty()){
                    inputtacgia.setError("tac gia khong dc de trong");
                    return;
                }
               else if (giabia.getText().toString().isEmpty()){
                    inputgiabia.setError("gia bia khong dc de trong");
                    return;
                }
               else if (soluong.getText().toString().isEmpty()){
                    inputsoluong.setError("so luong khong dc de trong");
                    return;
                }
               else if (mota.getText().toString().isEmpty()){
                    inputmota.setError("mo ta khong dc de trong");
                    return;
                }

          */
                    nd = new Sach(masach1,matheloaisach,tensach1,tacgia1,nxb1,giabia1,soluong1,mota1);
                    dao.insert(nd);




            }
        });
        btnhuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                masach.setText("");
                tensach.setText("");
                nhanxb.setText("");
                tacgia.setText("");
                giabia.setText("");
                soluong.setText("");
                mota.setText("");
            }
        });

    }


    public static void capnhatLV(){

        spintenloaimaloai.notifyDataSetChanged();
    }
}
