package com.example.bookmanager1.Sach;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.bookmanager1.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class SachFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sach,container,false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FragmentTransaction transaction=getFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container_sach,new ThemSachFragment());
        transaction.commit();
        BottomNavigationView bottomnav=view.findViewById(R.id.bottom_navi_sach);
        bottomnav.setOnNavigationItemSelectedListener(Navlister);

    }
    BottomNavigationView.OnNavigationItemSelectedListener Navlister=new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment=null;
            switch (menuItem.getItemId()){
                case R.id.nav_themsach:
                    selectedFragment=new ThemSachFragment();
                    break;
                case R.id.nav_showsach:
                    selectedFragment=new ShowSachFragment();
                    break;
            }
            FragmentTransaction transaction=getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container_sach,selectedFragment);
            transaction.commit();
            return true;
        }
    };
}
