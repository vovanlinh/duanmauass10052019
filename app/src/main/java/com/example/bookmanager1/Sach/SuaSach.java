package com.example.bookmanager1.Sach;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cursoradapter.widget.SimpleCursorAdapter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import android.widget.Spinner;

import com.example.bookmanager1.Adapter.spinTenTheLoaiSach;
import com.example.bookmanager1.DAO.ThemSachDao;
import com.example.bookmanager1.DAO.ThemTheLoaiDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.Sach;
import com.example.bookmanager1.model.Theloai;

import java.util.ArrayList;

public class SuaSach extends AppCompatActivity {
    Spinner spntentheloai;
    EditText masach,tensach,nhanxb,tacgia,soluong,giabia,mota;
    Button btnsua,btnhuy;
    ThemSachDao dao;
    Sach sach;
    //spin
    ThemTheLoaiDao ldao;
    ArrayList<Theloai> danhsachtenlt;
    static spinTenTheLoaiSach spintenloaimaloaisua;
    String matheloaisach="";
    String matheloaisachsua="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sua_sach);
        //anhxa
        masach=findViewById(R.id.edtmasachsua);
        tensach=findViewById(R.id.edttensachsua);
        nhanxb=findViewById(R.id.edtnxbsua);
        tacgia=findViewById(R.id.edttentacgiasua);
        giabia=findViewById(R.id.edtgiabiasua);
        soluong=findViewById(R.id.edtsoluongsua);
        btnsua=findViewById(R.id.btnsuasach);
        btnhuy=findViewById(R.id.btnhuysachsua);
        mota=findViewById(R.id.edtmotasua);
        spntentheloai=findViewById(R.id.spntheloaisua);
        //----------------
        ldao=new ThemTheLoaiDao(SuaSach.this);
        danhsachtenlt=new ArrayList<>();
        danhsachtenlt= (ArrayList<Theloai>) ldao.getTenTheLoaiSua();
        spintenloaimaloaisua=new spinTenTheLoaiSach(SuaSach.this,danhsachtenlt);
        spntentheloai.setAdapter(spintenloaimaloaisua);


        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("suaSach");
        if (bundle != null) {
            String masachsua = bundle.getString("masach", "");
            final String theloaisua = bundle.getString("matlsach", "");
            String tensua = bundle.getString("tensach", "");
            String tacgiasua = bundle.getString("tacgiasach", "");
            String nxbsua = bundle.getString("nxbsach", "");
            String giabiasua = bundle.getString("giabiasach", "");
            String soluongsua = bundle.getString("soluongsach", "");
            String motasua = bundle.getString("motasach", "");

            //final int machontl= Integer.parseInt(theloaisua);
         //   String myString = "some value"; //the value you want the position for
          //  if (theloaisua != null) {
           //     int spinnerPosition = spintenloaimaloaisua
            //spntentheloai.setSelection(Integer.parseInt(theloaisua));
           /*
            spntentheloai.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    spntentheloai.setSelection(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            */
         //
            //   }


            //dua data len EditText
            masach.setText(masachsua);
            tensach.setText(tensua);
            tacgia.setText(tacgiasua);
            nhanxb.setText(nxbsua);
            giabia.setText(giabiasua);
            soluong.setText(soluongsua);
            mota.setText(motasua);
          //  matheloaisachsua=danhsachtenlt.get(Integer.parseInt(theloaisua)).getMaTL();
            // khoa EditText MaSach

            if (!masachsua.isEmpty()) {
                masach.setEnabled(false);
            }
        }
        dao = new ThemSachDao(this);
        btnsua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String clma = masach.getText().toString();
                String clten = tensach.getText().toString();
                String cltacgia = tacgia.getText().toString();
                String clnhaxb = nhanxb.getText().toString();
                String clgiabia = giabia.getText().toString();
                String clsoluong = soluong.getText().toString();
                String clmota=mota.getText().toString();
                matheloaisach=danhsachtenlt.get(spntentheloai.getSelectedItemPosition()).getMaTL()+"";
                Sach sach = new Sach(clma,matheloaisach,clten,cltacgia,clnhaxb,clgiabia,clsoluong,clmota);

                dao.update(sach);
                finish();
                return;
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();

        capnhatLVsua();
    }
    public static void capnhatLVsua(){

        spintenloaimaloaisua.notifyDataSetChanged();
    }


}
