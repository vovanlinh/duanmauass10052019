package com.example.bookmanager1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.bookmanager1.model.HoaDonChiTiet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

public class ThongkeFragment extends Fragment {
TextView txttiensach;
private DatabaseReference mDatabase;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_thongke,container,false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
txttiensach=view.findViewById(R.id.txttiensachnam);
mDatabase= FirebaseDatabase.getInstance().getReference().child("HoaDonChiTiet");
mDatabase.addValueEventListener(new ValueEventListener() {
    int sum=0;

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        for (DataSnapshot ds:dataSnapshot.getChildren()){
            Map<String,Object>map=(Map<String, Object>)ds.getValue();
            Object price=map.get("giaSach");
            Object sl=map.get("soLuongMua");
            int pvalue=Integer.parseInt(String.valueOf(price));
           int slvalue=Integer.parseInt(String.valueOf(sl));
sum+=pvalue*slvalue;
txttiensach.setText(String.valueOf(sum)+"vnd");

        }

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
});

    }

}
