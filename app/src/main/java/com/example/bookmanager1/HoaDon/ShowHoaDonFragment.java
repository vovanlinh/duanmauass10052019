package com.example.bookmanager1.HoaDon;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bookmanager1.Adapter.HoaDonAdaptercv;
import com.example.bookmanager1.DAO.HoaDonDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.HoaDon;

import java.util.ArrayList;

public class ShowHoaDonFragment extends Fragment {
    RecyclerView lv;
    ArrayList<HoaDon> list;
    public static HoaDonAdaptercv adapter;
    static HoaDonDao dao;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_showhoadon,container,false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lv = view.findViewById(R.id.lv_showhoadon);
        list = new ArrayList<>();
        dao = new HoaDonDao(getContext());
        list = (ArrayList<HoaDon>)dao.getHoaDon();
        LinearLayoutManager mlinerlayoutmanager=new LinearLayoutManager(getContext());
        lv.setLayoutManager(mlinerlayoutmanager);
        adapter = new HoaDonAdaptercv(getContext(), list);

        lv.setAdapter(adapter);


    }
    public static void xoaHoaDon(HoaDon s){

        dao.delete(s);
        capnhatLV();

    }

    public static void capnhatLV(){

        adapter.notifyDataSetChanged();
    }


}
