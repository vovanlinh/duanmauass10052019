package com.example.bookmanager1.HoaDon;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.bookmanager1.DAO.HoaDonDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.HoaDon;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ThemHoaDonFragment extends Fragment {
Button btnpickdate,btnthemhoadon;
EditText edtdatehd,edtmahoadon;
TextInputLayout inputstt,inputngay;
    HoaDon nd;
    HoaDonDao dao;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_themhoadon,container,false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnpickdate=view.findViewById(R.id.btnpicdate);
        btnthemhoadon=view.findViewById(R.id.btnthemhoadon);
        edtmahoadon=view.findViewById(R.id.edtstthoadon);
        edtdatehd=view.findViewById(R.id.edtngaymua);
        inputstt=view.findViewById(R.id.inputstthd);
        inputngay=view.findViewById(R.id.inputngayhd);
        btnpickdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar=Calendar.getInstance();
                int ngay=calendar.get(Calendar.DATE);
                int thang=calendar.get(Calendar.MONTH);
                int nam=calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(year,month,dayOfMonth);
                        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
                        edtdatehd.setText(simpleDateFormat.format(calendar.getTime()));
                    }
                },nam,thang,ngay);
                datePickerDialog.show();
            }
        });
        dao = new HoaDonDao(getContext());
        btnthemhoadon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mahd = edtmahoadon.getText().toString();

                  String  datehd =edtdatehd.getText().toString();
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                if (edtmahoadon.getText().toString().isEmpty()

                ) {
                    inputstt.setError("STT khong dc de trong");
                    return;
                }
                try {
                    format.parse(edtdatehd.getText().toString());
                } catch (ParseException e) {
                    inputngay.setError("Dinh dang khong hop le");
                    e.printStackTrace();
                    return;
                }

                nd = new HoaDon(mahd,datehd);
                dao.insert(nd);

            }
        });

    }

}
