package com.example.bookmanager1.HoaDon;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.bookmanager1.Adapter.HoaDonChiTietAdaptercv;
import com.example.bookmanager1.DAO.HoaDonChiTietDao;
import com.example.bookmanager1.DAO.ThemSachDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.HoaDon;
import com.example.bookmanager1.model.HoaDonChiTiet;
import com.example.bookmanager1.model.Sach;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

public class ThemHoaDonChiTiet extends AppCompatActivity {
    Button btnback,btnthemhdct;
    RecyclerView lv;
EditText mahd,mahdct,soluong;
EditText masach;
TextInputLayout inputhdct,inputms,inputsl;
    HoaDonChiTiet nd;
    static HoaDonChiTietDao dao;
    ArrayList<HoaDonChiTiet> list;
    Sach sach;
    public static HoaDonChiTietAdaptercv adapter;
    ArrayList<String> dulieusach;
    ThemSachDao sdao;
String giabia,mas,sls;
int gias=0;
    String thanhtien;
    private DatabaseReference mDatabase;
    ArrayList<String> lists;
    static ArrayAdapter<String>adapters;
    String mahoadon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_hoa_don_chi_tiet);
       // setTitle("Hoa don chi tiet");
        anhxa();
        setonclick();
        lv =findViewById(R.id.rcvhdct);
        sdao=new ThemSachDao(this);

//hoadon
        list = new ArrayList<HoaDonChiTiet>();
        dao = new HoaDonChiTietDao(this);
        list = (ArrayList<HoaDonChiTiet>)dao.getIdHd(mahoadon);

        LinearLayoutManager mlinerlayoutmanager=new LinearLayoutManager(this);
        lv.setLayoutManager(mlinerlayoutmanager);
        adapter = new HoaDonChiTietAdaptercv(this, list);
        lv.setAdapter(adapter);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("themHDCT");
        if (bundle != null) {
             mahoadon = bundle.getString("mahoadon", "");

            //dua data len EditText
            mahd.setText(mahoadon);

            //  matheloaisachsua=danhsachtenlt.get(Integer.parseInt(theloaisua)).getMaTL();
            // khoa EditText MaSach

            if (!mahoadon.isEmpty()) {
                mahd.setEnabled(false);
            }
            //hoadon
            list = new ArrayList<HoaDonChiTiet>();
            dao = new HoaDonChiTietDao(this);
            list = (ArrayList<HoaDonChiTiet>)dao.getIdHd(mahoadon);
            adapter = new HoaDonChiTietAdaptercv(this, list);
            lv.setAdapter(adapter);
        }

    }
        private void setonclick () {
            btnback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            btnthemhdct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                //    Sach sach=new Sach(masach.getText().toString(),new String(),new String(),new String(),new String(),new String(),new String());
                   final String mahoadonthem = mahd.getText().toString();
final String masachthem=masach.getText().toString();
                    final String mahdctthem = mahdct.getText().toString();

                    final String soluongthem = soluong.getText().toString();
                    if (mahdct.getText().toString().isEmpty()){
                        inputhdct.setError("Ma hd ko duoc de trong");
                        return;
                    }
                    if (masach.getText().toString().isEmpty()){
                        inputms.setError("So luong ko duoc de trong");
                        return;
                    }
                    if (soluong.getText().toString().isEmpty()){
                        inputsl.setError("So luong ko duoc de trong");
                        return;
                    }
                    mDatabase= FirebaseDatabase.getInstance().getReference().child("Sach");

                    mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                for (DataSnapshot ds:dataSnapshot.getChildren()){
                                    Map<String,Object> map=(Map<String, Object>)ds.getValue();
                                    Object ms=map.get("maSach");
                                    Object tl=map.get("maTL");
                                    Object td=map.get("tieude");
                                    Object tg=map.get("tacgia");
                                    Object nx=map.get("nxb");
                                    Object gb=map.get("giabia");
                                    Object sl=map.get("soluong");
                                    Object mt=map.get("mota");
                                    String msvalue= String.valueOf(ms);
                                    String tlvalue= String.valueOf(tl);
                                    String tdvalue= String.valueOf(td);
                                    String tgvalue= String.valueOf(tg);
                                    String nxvalue= String.valueOf(nx);
                                    String gbvalue= String.valueOf(gb);
                                    String slvalue= String.valueOf(sl);
                                    String mtvalue=String.valueOf(mt);

                                    if (msvalue.equals(masach.getText().toString())){
                                        thanhtien= String.valueOf(gias*Integer.parseInt(soluongthem));
                                        String slmoi=Integer.parseInt(slvalue)-Integer.parseInt(soluongthem)+"";
                                        int slsmoi= Integer.parseInt(slmoi);
                                        if (slsmoi<0){inputms.setError("Khong du sach");
                                            break;
                                        }
                                        //         thanhtien= String.valueOf(Integer.parseInt(s.getGiabia())*Integer.parseInt(soluongthem));
                                        nd = new HoaDonChiTiet(mahoadonthem,mahdctthem,masachthem,soluongthem,gbvalue);
                                        dao.insert(nd);


                                        sach=new Sach(msvalue,tlvalue,tdvalue,tgvalue,nxvalue,gbvalue,slmoi,mtvalue);

                                        sdao.update(sach);
                                        mahdct.setText("");
                                        masach.setText("");
                                        soluong.setText("");
                                        finish();
                                        break;
                                    } else if (!msvalue.equals(masach.getText().toString())) {
                                        inputms.setError("Sai ma sach!");
                                    }


                                }
                            }





                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                }
            });
        }

        private void anhxa () {
            btnback = findViewById(R.id.btnwedback);
            mahd=findViewById(R.id.edtmahoadonhdct);
            mahdct=findViewById(R.id.edtmahoadonchitiet);
            masach=findViewById(R.id.edtmasachhdct);
            soluong=findViewById(R.id.edtsoluonghdct);
            btnthemhdct=findViewById(R.id.btnthemhdct);
            inputhdct=findViewById(R.id.inputhdct);
            inputms=findViewById(R.id.inputms);
            inputsl=findViewById(R.id.inputsl);
        }
    public static void capnhatLVhdct(){

        adapter.notifyDataSetChanged();
    }


    //themhoadon
    public static void capnhatLVTL(){

        adapters.notifyDataSetChanged();
    }
    public static void xoaHoaDon(HoaDonChiTiet s){

        dao.delete(s);
        capnhatLVhdct();

    }

    @Override
    protected void onResume() {
        super.onResume();
        capnhatLVhdct();
    }
}
