package com.example.bookmanager1.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bookmanager1.DAO.ThemTheLoaiDao;
import com.example.bookmanager1.MainActivity;
import com.example.bookmanager1.R;
import com.example.bookmanager1.TheLoaiSach.ShowTheLoaiSachFragment;
import com.example.bookmanager1.TheLoaiSach.SuaTheLoaiSach;
import com.example.bookmanager1.model.Theloai;

import java.util.ArrayList;

public class TheLoaiAdaptercv extends RecyclerView.Adapter<TheLoaiAdaptercv.ViewHolder> {
    Context context;
    ArrayList<Theloai> dulieund;
    ThemTheLoaiDao dao;
    public TheLoaiAdaptercv(Context context, ArrayList<Theloai> dulieund) {
      //  super(context,0,dulieund);
        this.context = context;
        this.dulieund = dulieund;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView matl,tentl;
        public ImageView popupxoasua;
        public ViewHolder(View itemview){
            super(itemview);
            matl=itemview.findViewById(R.id.cvtxt_idTL);
            tentl=itemview.findViewById(R.id.cvtxt_tenTL);
            popupxoasua=itemview.findViewById(R.id.popupxoasuatheloai);


        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
View v = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.cardview_theloai,viewGroup,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
      final Theloai nd=dulieund.get(i);
      if (nd!=null){
          viewHolder.matl.setText(nd.getMaTL());
          viewHolder.tentl.setText(nd.getTenTL());

      }
        viewHolder.popupxoasua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                dao = new ThemTheLoaiDao(context);
                PopupMenu popup=new PopupMenu(v.getContext(),v);// co the de view thay cho text view van dc
                MenuInflater inflater=popup.getMenuInflater();
                inflater.inflate(R.menu.poup_menu_kt,popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.poupXoakt:
                                AlertDialog.Builder builder=new AlertDialog.Builder(context,android.R.style.Theme_DeviceDefault_Light_Dialog);
                                builder.setTitle("Ban co chac muon xoa");
                                builder.setMessage("Hay lua chon ben duoi de xac nhan");
                                builder.setIcon(android.R.drawable.ic_dialog_alert);
                                builder.setPositiveButton("Co", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int w) {
// Truy cap den bien nao do trong MainActivity ((MainActivity)context)
                                        dao.delete(nd);
                                        TheLoaiAdaptercv.this.notifyDataSetChanged();
                                    }
                                });
                                builder.setNegativeButton("Khong", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int w) {

                                    }
                                });
                                builder.show();

                                break;
                            case R.id.poupSuakt:
                              /*
                                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                                //gắn layout vào view
                                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                                final View view1 = inflater.inflate(R.layout.activity_sua_the_loai_sach, null);
                                EditText matl=view1.findViewById(R.id.edmatheloaisua);
                                EditText tentl=view1.findViewById(R.id.edtentheloaisua);
                                EditText vitritl=view1.findViewById(R.id.edvitritheloaisua);
                                EditText motatl=view1.findViewById(R.id.edmotatheloaisua);
*/ Intent intent = new Intent(context, SuaTheLoaiSach.class);

                                Bundle bundle = new Bundle();
                                bundle.putString("matheloai",nd.maTL);
                                bundle.putString("tentheloai",nd.tenTL);
                                bundle.putString("vitritheloai",nd.vitriTL);
                                bundle.putString("motatheloai",nd.motaTL);

                                intent.putExtra("suaTheloai",bundle);

                                ((MainActivity)context).startActivity(intent);
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });

    }


    @Override
    public int getItemCount() {
        return dulieund.size();
    }

}

