package com.example.bookmanager1.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.bookmanager1.DAO.ThemTheLoaiDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.Sach.SuaSach;
import com.example.bookmanager1.model.Theloai;


import java.util.ArrayList;

public class spinTenTheLoaiSach extends BaseAdapter{
    Context context;
    ArrayList<Theloai> dulieu;
    ThemTheLoaiDao dao;
    public spinTenTheLoaiSach(Context context, ArrayList<Theloai> dulieu) {
        this.context = context;
        this.dulieu = dulieu;
        dao=new ThemTheLoaiDao(context);
    }
    @Override
    public int getCount() {
        return dulieu.size();
    }

    @Override
    public Object getItem(int position) {
        return dulieu.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);// cho get co 2 lua chon
            convertView=layoutInflater.inflate(R.layout.sp_tentheloai,null);

        }

TextView tvspinmatenloai=convertView.findViewById(R.id.spinmatl);
        TextView tvspintenloai=convertView.findViewById(R.id.tvspintenloai);

        tvspintenloai.setText(dulieu.get(position).tenTL);
tvspinmatenloai.setText(dulieu.get(position).maTL);

        return convertView;
    }
}
