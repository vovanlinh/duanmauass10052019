package com.example.bookmanager1.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bookmanager1.DAO.HoaDonChiTietDao;
import com.example.bookmanager1.DAO.HoaDonDao;
import com.example.bookmanager1.DAO.ThemSachDao;
import com.example.bookmanager1.HoaDon.ShowHoaDonFragment;
import com.example.bookmanager1.HoaDon.ThemHoaDonChiTiet;
import com.example.bookmanager1.MainActivity;
import com.example.bookmanager1.R;
import com.example.bookmanager1.Sach.ThemSachFragment;
import com.example.bookmanager1.model.HoaDon;
import com.example.bookmanager1.model.HoaDonChiTiet;
import com.example.bookmanager1.model.Sach;

import java.util.ArrayList;
import java.util.List;

public class HoaDonChiTietAdaptercv extends RecyclerView.Adapter<HoaDonChiTietAdaptercv.ViewHolder> {
    Context context;
    ArrayList<HoaDonChiTiet> dulieund;
    HoaDonDao dao;

    HoaDonChiTietDao daosach;
    List<Sach> booklist;
    ArrayList<Sach> dulieusach;
ThemSachDao sdao;
int  giabiahdct=0;
int thanhtienhdct=0;
    public HoaDonChiTietAdaptercv(Context context, ArrayList<HoaDonChiTiet> dulieund) {
        this.context = context;
        this.dulieund = dulieund;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView masach,soluong,giabia,thanhtien;
        public ImageView popupxoasua;
        public ViewHolder(View itemview){
            super(itemview);

            masach=itemview.findViewById(R.id.txtmasachhdct);
            popupxoasua=itemview.findViewById(R.id.popupxoasuahdct);
            soluong=itemview.findViewById(R.id.txtsoluonghdct);
            giabia=itemview.findViewById(R.id.txtgiabiahdct);
            thanhtien=itemview.findViewById(R.id.txtthanhtienhdct);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
View v = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.item_hdct,viewGroup,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        final HoaDonChiTiet nd = dulieund.get(i);

int gia;
gia=Integer.parseInt(nd.getGiaSach())*Integer.parseInt(nd.getSoLuongMua());

        if (nd != null) {

            viewHolder.masach.setText(nd.getMaSach());
            viewHolder.soluong.setText(nd.soLuongMua);
               viewHolder.giabia.setText(nd.getGiaSach()+"vnd");
            viewHolder.thanhtien.setText(gia+"vnd");

        }


        viewHolder.popupxoasua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(v.getContext(), v);// co the de view thay cho text view van dc
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.poup_menu_kt, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.poupXoakt:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context, android.R.style.Theme_DeviceDefault_Light_Dialog);
                                builder.setTitle("Ban co chac muon xoa");
                                builder.setMessage("Hay lua chon ben duoi de xac nhan");
                                builder.setIcon(android.R.drawable.ic_dialog_alert);
                                builder.setPositiveButton("Co", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int w) {
// Truy cap den bien nao do trong MainActivity ((MainActivity)context)
                                           ThemHoaDonChiTiet.xoaHoaDon(nd);
                                    }
                                });
                                builder.setNegativeButton("Khong", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int w) {

                                    }
                                });
                                builder.show();

                                break;
                            case R.id.poupSuakt:
                                break;

                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return dulieund.size();
    }


}

