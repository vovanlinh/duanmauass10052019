package com.example.bookmanager1.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bookmanager1.DAO.ThemNguoiDungDao;
import com.example.bookmanager1.MainActivity;
import com.example.bookmanager1.NguoiDung.DoiThongTinNguoiDung;
import com.example.bookmanager1.NguoiDung.ShowNguoiDungFragment;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.Nguoidung;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class NguoiDungAdaptercv extends RecyclerView.Adapter<NguoiDungAdaptercv.ViewHolder> {
    Context context;
    ArrayList<Nguoidung> dulieund;
    ThemNguoiDungDao dao;

    public NguoiDungAdaptercv(Context context, ArrayList<Nguoidung> dulieund) {
        this.context = context;
        this.dulieund = dulieund;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView sdt,fullname;
        public ImageView popupxoasua;
        public ViewHolder(View itemview){
            super(itemview);

            sdt=itemview.findViewById(R.id.cvtxt_sdt);
            popupxoasua=itemview.findViewById(R.id.popupxoasuand);
            fullname=itemview.findViewById(R.id.cvtxt_hoten);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
View v = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.cardview_nguoidung,viewGroup,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
      final Nguoidung nd=dulieund.get(i);
      if (nd!=null){

          viewHolder.sdt.setText(nd.sdtnd);
          viewHolder.fullname.setText(nd.hotennd);

      }
        viewHolder.popupxoasua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup=new PopupMenu(v.getContext(),v);// co the de view thay cho text view van dc
                MenuInflater inflater=popup.getMenuInflater();
                inflater.inflate(R.menu.poup_menu_kt,popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.poupXoakt:
                                AlertDialog.Builder builder=new AlertDialog.Builder(context,android.R.style.Theme_DeviceDefault_Light_Dialog);
                                builder.setTitle("Ban co chac muon xoa");
                                builder.setMessage("Hay lua chon ben duoi de xac nhan");
                                builder.setIcon(android.R.drawable.ic_dialog_alert);
                                builder.setPositiveButton("Co", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int w) {
// Truy cap den bien nao do trong MainActivity ((MainActivity)context)
                                        ShowNguoiDungFragment.xoaNguoidung(nd);
                                    }
                                });
                                builder.setNegativeButton("Khong", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int w) {

                                    }
                                });
                                builder.show();

                                break;
                            case R.id.poupSuakt:
                                Intent intent = new Intent(context, DoiThongTinNguoiDung.class);

                                Bundle bundle = new Bundle();
                                bundle.putString("us",nd.usernd);
                                bundle.putString("ps",nd.passnd);
                                bundle.putString("rps",nd.passnd);
                                bundle.putString("sd",nd.sdtnd);
                                bundle.putString("ht",nd.hotennd);

                                intent.putExtra("suaThongTin",bundle);

                                ((MainActivity)context).startActivity(intent);
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return dulieund.size();
    }


}

