package com.example.bookmanager1.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bookmanager1.DAO.ThemSachDao;
import com.example.bookmanager1.DAO.ThemTheLoaiDao;
import com.example.bookmanager1.MainActivity;
import com.example.bookmanager1.R;
import com.example.bookmanager1.Sach.ShowSachFragment;
import com.example.bookmanager1.Sach.SuaSach;
import com.example.bookmanager1.TheLoaiSach.ShowTheLoaiSachFragment;
import com.example.bookmanager1.model.Sach;
import com.example.bookmanager1.model.Theloai;

import java.util.ArrayList;
import java.util.List;

public class SachAdaptercv extends RecyclerView.Adapter<SachAdaptercv.ViewHolder>implements Filterable {
    Context context;
    ArrayList<Sach> dulieund;
    ThemSachDao dao;
    List<Sach> arrSortSach;
    private Filter sachFilter;
    public SachAdaptercv(Context context, ArrayList<Sach> dulieund) {
      //  super(context,0,dulieund);
        this.context = context;
        this.dulieund = dulieund;
        this.arrSortSach=dulieund;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tensach,slsach;
        public ImageView popupxoasua;
        public ViewHolder(View itemview){
            super(itemview);
            tensach=itemview.findViewById(R.id.cvtxt_tensach);
            slsach=itemview.findViewById(R.id.cvtxt_soluongsach);
            popupxoasua=itemview.findViewById(R.id.popupxoasuasach);


        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
View v = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.cardview_sach,viewGroup,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

      final Sach nd=dulieund.get(i);
      if (nd!=null){
          viewHolder.tensach.setText(nd.getTieude());
          viewHolder.slsach.setText(nd.getSoluong());

      }
        viewHolder.popupxoasua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup=new PopupMenu(v.getContext(),v);// co the de view thay cho text view van dc
                MenuInflater inflater=popup.getMenuInflater();
                inflater.inflate(R.menu.poup_menu_kt,popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.poupXoakt:
                                AlertDialog.Builder builder=new AlertDialog.Builder(context,android.R.style.Theme_DeviceDefault_Light_Dialog);
                                builder.setTitle("Ban co chac muon xoa");
                                builder.setMessage("Hay lua chon ben duoi de xac nhan");
                                builder.setIcon(android.R.drawable.ic_dialog_alert);
                                builder.setPositiveButton("Co", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int w) {
// Truy cap den bien nao do trong MainActivity ((MainActivity)context)

                                        ShowSachFragment.xoaSach(nd);
                                    }
                                });
                                builder.setNegativeButton("Khong", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int w) {

                                    }
                                });
                                builder.show();
                                break;
                            case R.id.poupSuakt:
                                Intent intent = new Intent(context, SuaSach.class);

                                Bundle bundle = new Bundle();
                                bundle.putString("masach",nd.maSach);
                                bundle.putString("matlsach",nd.maTL);
                                bundle.putString("tensach",nd.tieude);
                                bundle.putString("tacgiasach",nd.tacgia);
                                bundle.putString("nxbsach",nd.nxb);
                                bundle.putString("giabiasach",nd.giabia);
                                bundle.putString("soluongsach",nd.soluong);
                                bundle.putString("motasach",nd.mota);
                                intent.putExtra("suaSach",bundle);

                                ((MainActivity)context).startActivity(intent);
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });

    }


    @Override
    public int getItemCount() {
        return dulieund.size();
    }

    public void changeDataset(List<Sach> items){
        this.dulieund = (ArrayList<Sach>) items;
        notifyDataSetChanged();
    }
    public void resetData() {
        dulieund = (ArrayList<Sach>) arrSortSach;
    }
    @Override
    public Filter getFilter() {
        if (sachFilter == null)
            sachFilter = new CustomFilter();
        return sachFilter;
    }
    private class CustomFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint == null || constraint.length() == 0) {
                results.values = arrSortSach;
                results.count = arrSortSach.size();
            }
            else {
                List<Sach> lsSach = new ArrayList<Sach>();
                for (Sach p : dulieund) {
                    if
                    (p.getTieude().toUpperCase().startsWith(constraint.toString().toUpperCase()))
                        lsSach.add(p);
                }
                results.values = lsSach;
                results.count = lsSach.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count == 0){}

            else {
                dulieund = (ArrayList<Sach>) results.values;
                notifyDataSetChanged();
            }
        }
    }
}

