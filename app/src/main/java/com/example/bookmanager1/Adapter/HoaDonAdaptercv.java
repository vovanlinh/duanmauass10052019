package com.example.bookmanager1.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bookmanager1.DAO.ThemNguoiDungDao;
import com.example.bookmanager1.HoaDon.ShowHoaDonFragment;
import com.example.bookmanager1.HoaDon.ThemHoaDonChiTiet;
import com.example.bookmanager1.MainActivity;
import com.example.bookmanager1.NguoiDung.ShowNguoiDungFragment;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.HoaDon;
import com.example.bookmanager1.model.Nguoidung;

import java.util.ArrayList;

public class HoaDonAdaptercv extends RecyclerView.Adapter<HoaDonAdaptercv.ViewHolder> {
    Context context;
    ArrayList<HoaDon> dulieund;
    HoaDon dao;

    public HoaDonAdaptercv(Context context, ArrayList<HoaDon> dulieund) {
        this.context = context;
        this.dulieund = dulieund;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mahoadon,ngaymua;
        public ImageView popupxoasua;
        public ViewHolder(View itemview){
            super(itemview);

            mahoadon=itemview.findViewById(R.id.cvtxt_mahoadon);
            popupxoasua=itemview.findViewById(R.id.popupxoasuahoadon);
            ngaymua=itemview.findViewById(R.id.cvtxt_ngaymua);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
View v = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.cardview_hoadon,viewGroup,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
      final HoaDon nd=dulieund.get(i);
      if (nd!=null){

          viewHolder.mahoadon.setText(nd.maHoaDon);
          viewHolder.ngaymua.setText(nd.ngayMua);

      }
        viewHolder.popupxoasua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup=new PopupMenu(v.getContext(),v);// co the de view thay cho text view van dc
                MenuInflater inflater=popup.getMenuInflater();
                inflater.inflate(R.menu.poup_menu_hoadon,popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.poupXoahd:
                                AlertDialog.Builder builder=new AlertDialog.Builder(context,android.R.style.Theme_DeviceDefault_Light_Dialog);
                                builder.setTitle("Ban co chac muon xoa");
                                builder.setMessage("Hay lua chon ben duoi de xac nhan");
                                builder.setIcon(android.R.drawable.ic_dialog_alert);
                                builder.setPositiveButton("Co", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int w) {
// Truy cap den bien nao do trong MainActivity ((MainActivity)context)
                                        ShowHoaDonFragment.xoaHoaDon(nd);
                                    }
                                });
                                builder.setNegativeButton("Khong", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int w) {

                                    }
                                });
                                builder.show();

                                break;
                            case R.id.poupSuahd:
                                break;
                            case R.id.poupThemhdct:
                                Intent intent = new Intent(context, ThemHoaDonChiTiet.class);

                                Bundle bundle = new Bundle();
                                bundle.putString("mahoadon",nd.maHoaDon);

                                intent.putExtra("themHDCT",bundle);

                                ((MainActivity)context).startActivity(intent);
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return dulieund.size();
    }


}

