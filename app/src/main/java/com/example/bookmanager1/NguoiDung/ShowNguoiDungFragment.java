package com.example.bookmanager1.NguoiDung;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bookmanager1.Adapter.NguoiDungAdaptercv;
import com.example.bookmanager1.DAO.ThemNguoiDungDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.Nguoidung;

import java.util.ArrayList;

public class ShowNguoiDungFragment extends Fragment {
    RecyclerView lv;
    ArrayList<Nguoidung> list;

    static ThemNguoiDungDao dao;
    public static NguoiDungAdaptercv adapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shownguoidung,container,false);


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lv = view.findViewById(R.id.lv_shownd);
      list = new ArrayList<>();
        dao = new ThemNguoiDungDao(getContext());
        list = (ArrayList<Nguoidung>)dao.getNguoiDung();
        LinearLayoutManager mlinerlayoutmanager=new LinearLayoutManager(getContext());
        lv.setLayoutManager(mlinerlayoutmanager);
        adapter = new NguoiDungAdaptercv(getContext(), list);

        lv.setAdapter(adapter);


    }
    public static void xoaNguoidung(Nguoidung s){

        dao.delete(s);
        capnhatLV();

    }

    public static void capnhatLV(){

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
