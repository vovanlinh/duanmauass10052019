package com.example.bookmanager1.NguoiDung;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.bookmanager1.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class NguoidungFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View chifragment=inflater.inflate(R.layout.fragment_nguoidung,container,false);
        return chifragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        FragmentTransaction transaction=getFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container_chi,new ThemNguoiDungFragment());
        transaction.commit();
        BottomNavigationView bottomnav=view.findViewById(R.id.bottom_navi);
        bottomnav.setOnNavigationItemSelectedListener(Navlister);

    }
     BottomNavigationView.OnNavigationItemSelectedListener Navlister=new BottomNavigationView.OnNavigationItemSelectedListener() {
       @Override
     public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
Fragment selectedFragment=null;
switch (menuItem.getItemId()){
      case R.id.nav_khoanchi:
        selectedFragment=new ThemNguoiDungFragment();
      break;
    case R.id.nav_loaichi:
      selectedFragment=new ShowNguoiDungFragment();
    break;
}
FragmentTransaction transaction=getFragmentManager().beginTransaction();
transaction.replace(R.id.fragment_container_chi,selectedFragment);
transaction.commit();
              return true;
        }
    };
}
