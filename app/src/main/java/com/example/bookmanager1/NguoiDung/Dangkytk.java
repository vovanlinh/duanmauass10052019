package com.example.bookmanager1.NguoiDung;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.bookmanager1.DAO.ThemNguoiDungDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.Nguoidung;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

public class Dangkytk extends AppCompatActivity {
    EditText username,pass,repass,sdt,hoten;
    Button btnthem,btnhuy,btnshow;
    TextInputLayout txtrepass,inputuser,inputsdt,inputhoten,inputpass;
    Nguoidung nd;
    Button btnback;
    private DatabaseReference mDatabase;
    ThemNguoiDungDao dao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dangkytk);
        mDatabase= FirebaseDatabase.getInstance().getReference().child("Nguoidung");
        username=findViewById(R.id.edtusernamendlg);
        pass=findViewById(R.id.edtpaswordndlg);
        repass=findViewById(R.id.edtrepasswordndlg);
        sdt=findViewById(R.id.edtsdtndlg);
        hoten=findViewById(R.id.edttenndlg);
        btnthem=findViewById(R.id.btnthemndlg);
        btnhuy=findViewById(R.id.btnhuyndlg);
        btnback = findViewById(R.id.btnwedbacklg);
        //error
        txtrepass=findViewById(R.id.txtinputrepasslg);
        inputuser=findViewById(R.id.inputuserlg);
        inputsdt=findViewById(R.id.inputsdtlg);
        inputhoten=findViewById(R.id.inputhotenlg);
        inputpass=findViewById(R.id.inputpasslg);

        dao = new ThemNguoiDungDao(this);

        btnthem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final String usernd = username.getText().toString();
                final String passnd = pass.getText().toString();
                String repassnd = repass.getText().toString();
                final String sdtnd = sdt.getText().toString();
                final String hotennd = hoten.getText().toString();


                if  (!repassnd.equals(passnd)) {
                    txtrepass.setError("Sai mat khau");
                    return;
                }
                String MobilePattern = "[0-9]{10}";
                if (username.getText().toString().isEmpty()){
                    inputuser.setError("user name khong duoc de trong");
                    return ;
                }
                if (pass.getText().toString().isEmpty()){
                    inputpass.setError("pass khong duoc de trong");
                    return ;
                }
               else if (pass.getText().toString().length()<6){
                    inputpass.setError("pass phai 6 ki tu tro len");
                    return;
                }
               else if (repass.getText().toString().isEmpty()){
                    txtrepass.setError("repass khong duoc de trong");
                    return ;
                }
               else if (sdt.getText().toString().isEmpty()){
                    inputsdt.setError("sdt khong duoc de trong");
                    return ;
                }
               else if (hoten.getText().toString().isEmpty()){
                    inputhoten.setError("ho ten khong duoc de trong");
                    return ;
                }
                else if (!sdt.getText().toString().matches(MobilePattern)){
                    inputsdt.setError("SDT khong chinh xac");
                    return;
                }

            else {
                nd = new Nguoidung(usernd, passnd, sdtnd, hotennd);
                    dao.insert(nd);

                    username.setText("");
                    pass.setText("");
                    repass.setText("");
                    sdt.setText("");
                    hoten.setText("");
                }





            }
        });
        btnhuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username.setText("");
                pass.setText("");
                repass.setText("");
                sdt.setText("");
                hoten.setText("");
            }
        });
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}
