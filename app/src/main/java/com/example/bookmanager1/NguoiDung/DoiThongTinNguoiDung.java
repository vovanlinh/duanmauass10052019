package com.example.bookmanager1.NguoiDung;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.bookmanager1.DAO.ThemNguoiDungDao;
import com.example.bookmanager1.DAO.ThemTheLoaiDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.Nguoidung;
import com.example.bookmanager1.model.Theloai;
import com.google.android.material.textfield.TextInputLayout;

public class DoiThongTinNguoiDung extends AppCompatActivity {
    EditText username,pass,repass,sdt,hoten;
    Button btnthem,btnhuy,btnshow;
    TextInputLayout txtrepass,inputuser,inputsdt,inputhoten,inputpass;
    Nguoidung nd;
    ThemNguoiDungDao dao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doi_thong_tin_nguoi_dung);
        anhxa();
        setonclick();
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("suaThongTin");

        if (bundle != null) {
            String user = bundle.getString("us", "");
            String passtd = bundle.getString("ps", "");
            String repasstd = bundle.getString("rps", "");
            String sdttd = bundle.getString("sd", "");
            String hotentd = bundle.getString("ht", "");
            //dua data len EditText
            username.setText(user);
            pass.setText(passtd);
            repass.setText(repasstd);
            sdt.setText(sdttd);
hoten.setText(hotentd);
            // khoa EditText MaSach

            if (!user.isEmpty()) {
                username.setEnabled(false);
            }
        }
        dao = new ThemNguoiDungDao(this);
        btnthem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cluser = username.getText().toString();
                String clpass = pass.getText().toString();
                String clrepass = repass.getText().toString();
                String clsdt = sdt.getText().toString();
                String clhoten = hoten.getText().toString();
                if  (!clrepass.equals(clpass)) {
                    txtrepass.setError("Sai mat khau");
                    return;
                }
                String MobilePattern = "[0-9]{10}";
                if (username.getText().toString().isEmpty()){
                    inputuser.setError("user name khong duoc de trong");
                    return ;
                }
             else    if (pass.getText().toString().isEmpty()){
                    inputpass.setError("pass khong duoc de trong");
                    return ;
                }
               else if (pass.getText().toString().length()<6){
                    inputpass.setError("pass phai 6 ki tu tro len");
                    return;
                }
               else if (repass.getText().toString().isEmpty()){
                    txtrepass.setError("repass khong duoc de trong");
                    return ;
                }
                else if (repass.getText().toString().length()<6){
                    txtrepass.setError("pass phai 6 ki tu tro len");
                    return;
                }
               else if (sdt.getText().toString().isEmpty()){
                    inputsdt.setError("sdt khong duoc de trong");
                    return ;
                }
               else if (hoten.getText().toString().isEmpty()){
                    inputhoten.setError("ho ten khong duoc de trong");
                    return ;
                }
               else if (!sdt.getText().toString().matches(MobilePattern)){
                    inputsdt.setError("SDT khong chinh xac");
                    return;
                }
               else {
                Nguoidung nd = new Nguoidung(cluser,clpass,clsdt,clhoten);
                dao.update(nd);
                finish();}
            }
        });
        btnhuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setonclick() {


    }

    private void anhxa() {
        //anhxa
        username=findViewById(R.id.edtusernamendtd);
        pass=findViewById(R.id.edtpaswordndtd);
        repass=findViewById(R.id.edtrepasswordndtd);
        sdt=findViewById(R.id.edtsdtndtd);
        hoten=findViewById(R.id.edttenndtd);
        btnthem=findViewById(R.id.btnthemndtd);
        btnhuy=findViewById(R.id.btnhuyndtd);

        //error
        txtrepass=findViewById(R.id.txtinputrepasstd);
        inputuser=findViewById(R.id.inputusertd);
        inputsdt=findViewById(R.id.inputsdttd);
        inputhoten=findViewById(R.id.inputhotentd);
        inputpass=findViewById(R.id.inputpasstd);

    }
    @Override
    protected void onResume() {
        super.onResume();


    }
}
