package com.example.bookmanager1.NguoiDung;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.bookmanager1.DAO.ThemNguoiDungDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.Nguoidung;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

public class DoiMk extends AppCompatActivity {
    EditText username,pass,repass,passmoi2;
    Button btnthem,btnhuy,btnshow;
    TextInputLayout txtrepass,inputuser,inputpass,inputpassmoi2;
    Nguoidung nd;
    ThemNguoiDungDao dao;
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doi_mk);
        anhxa();
        dao = new ThemNguoiDungDao(this);
        mDatabase= FirebaseDatabase.getInstance().getReference().child("Nguoidung");
        btnthem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String cluser = username.getText().toString();
                final String clpass = pass.getText().toString();
                final String clrepass = repass.getText().toString();
                final String clrepass2 = passmoi2.getText().toString();
                if (username.getText().toString().isEmpty()){
                    inputuser.setError("user name khong duoc de trong");
                    return ;
                }
                if (pass.getText().toString().isEmpty()){
                    inputpass.setError("pass khong duoc de trong");
                    return ;
                }
                if (repass.getText().toString().isEmpty()){
                    txtrepass.setError("Mat khau moi khong dc de trong");
                    return ;
                }
                if (repass.getText().toString().length()<6){
                    txtrepass.setError("pass phai 6 ki tu tro len");
                    return;
                }
                if (passmoi2.getText().toString().isEmpty()){
                    inputpassmoi2.setError("Mat khau moi khong dc de trong");
                    return;
                }
                if (passmoi2.getText().toString().length()<6){
                    inputpassmoi2.setError("pass phai 6 ki tu tro len");
                    return;
                }
                if (!passmoi2.getText().toString().equals(repass.getText().toString())){
                    inputpassmoi2.setError("Mat khau moi khong chinh xac");
                    return;
                }
               if (passmoi2.getText().toString().isEmpty()){
                    inputpassmoi2.setError("Mat khau moi khong dc de trong");
                    return;
               }


                mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {


                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds:dataSnapshot.getChildren()){
                            Map<String,Object> map=(Map<String, Object>)ds.getValue();
                            Object price=map.get("usernd");
                            Object sl=map.get("sdtnd");
                            Object ht=map.get("hotennd");
                            Object ps=map.get("passnd");
String u= String.valueOf(price);
                            String sd= String.valueOf(sl);
                            String hten= String.valueOf(ht);
                            String pscu= String.valueOf(ps);

                            if (username.getText().toString().equals(u)&&pass.getText().toString().equals(pscu)){
                                Nguoidung nd = new Nguoidung(cluser,clrepass,sd,hten);
                                dao.update(nd);
                                finish();
                               return;
                            }
                            if (!username.getText().toString().equals(u)){
                                inputuser.setError("user name khong chinh xac");

                            }
                            if (!pass.getText().toString().equals(pscu)){
                                inputpass.setError("pass khong chinh xac");

                            }

                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });
        btnhuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username.setText("");
                passmoi2.setText("");
                pass.setText("");
                repass.setText("");
            }
        });
    }

    private void anhxa() {
        //anhxa
        username=findViewById(R.id.edtusernamenddoi);
        pass=findViewById(R.id.edtpaswordnddoi);
        repass=findViewById(R.id.edtrepasswordnddoi);
        passmoi2=findViewById(R.id.edtrepasswordnddoi2);

        btnthem=findViewById(R.id.btnthemnddoi);
        btnhuy=findViewById(R.id.btnhuynddoi);

        //error
        txtrepass=findViewById(R.id.txtinputrepassdoi);
        inputpassmoi2=findViewById(R.id.txtinputrepassdoi2);
        inputuser=findViewById(R.id.inputuserdoi);
        inputpass=findViewById(R.id.inputpassdoi);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
