package com.example.bookmanager1.NguoiDung;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.bookmanager1.Adapter.NguoiDungAdaptercv;
import com.example.bookmanager1.DAO.ThemNguoiDungDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.Nguoidung;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class ThemNguoiDungFragment extends Fragment {
EditText username,pass,repass,sdt,hoten;
Button btnthem,btnhuy,btnshow;
TextInputLayout txtrepass,inputuser,inputsdt,inputhoten,inputpass;
    Nguoidung nd;
    ThemNguoiDungDao dao;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_themnguoidung,container,false);


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//anhxa
       username=view.findViewById(R.id.edtusernamend);
       pass=view.findViewById(R.id.edtpaswordnd);
        repass=view.findViewById(R.id.edtrepasswordnd);
        sdt=view.findViewById(R.id.edtsdtnd);
       hoten=view.findViewById(R.id.edttennd);
        btnthem=view.findViewById(R.id.btnthemnd);
        btnhuy=view.findViewById(R.id.btnhuynd);
        btnshow=view.findViewById(R.id.btnshownd);
        //error
txtrepass=view.findViewById(R.id.txtinputrepass);
inputuser=view.findViewById(R.id.inputuser);
        inputsdt=view.findViewById(R.id.inputsdt);
        inputhoten=view.findViewById(R.id.inputhoten);
        inputpass=view.findViewById(R.id.inputpass);
// lay data tu bundle
/*

        }
*/


        dao = new ThemNguoiDungDao(getContext());

        btnthem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String usernd = username.getText().toString();
                String passnd = pass.getText().toString();
                String repassnd = repass.getText().toString();
                String sdtnd = sdt.getText().toString();
                String hotennd = hoten.getText().toString();

                checkvali();
if  (!repassnd.equals(passnd)) {
txtrepass.setError("Sai mat khau");
return;
}
                String MobilePattern = "[0-9]{10}";
                if (username.getText().toString().isEmpty()){
                    inputuser.setError("user name khong duoc de trong");
                    return ;
                }
                if (pass.getText().toString().isEmpty()){
                    inputpass.setError("pass khong duoc de trong");
                    return ;
                }
                if (pass.getText().toString().length()<6){
                    inputpass.setError("pass phai 6 ki tu tro len");
                    return;
                }
                if (repass.getText().toString().isEmpty()){
                    txtrepass.setError("repass khong duoc de trong");
                    return ;
                }
                if (repass.getText().toString().length()<6){
                    txtrepass.setError("pass phai 6 ki tu tro len");
                    return;
                }
                if (sdt.getText().toString().isEmpty()){
                    inputsdt.setError("sdt khong duoc de trong");
                    return ;
                }
                if (hoten.getText().toString().isEmpty()){
                    inputhoten.setError("ho ten khong duoc de trong");
                    return ;
                }
                if (!sdt.getText().toString().matches(MobilePattern)){
                    inputsdt.setError("SDT khong chinh xac");
                    return;
                }
    nd = new Nguoidung(usernd, passnd, sdtnd, hotennd);
    dao.insert(nd);

    username.setText("");
    pass.setText("");
    repass.setText("");
    sdt.setText("");
    hoten.setText("");

            }
        });
        btnhuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username.setText("");
                pass.setText("");
                repass.setText("");
                sdt.setText("");
                hoten.setText("");
            }
        });


    }



    private void finish() {
    }
public void checkvali(){


}
}
