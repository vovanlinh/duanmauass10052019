package com.example.bookmanager1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity2 extends AppCompatActivity {
    EditText tongthu,tongchi,tongdu;
    Button btntext;
    FirebaseDatabase database;
    DatabaseReference ref;
    Member member;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        anhxa();
        member=new Member();
        ref=FirebaseDatabase.getInstance().getReference().child("Member");
        btntext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int age=Integer.parseInt(tongthu.getText().toString().trim());
                Long ph=Long.parseLong(tongchi.getText().toString().trim());
                member.setName(tongdu.getText().toString().trim());
                member.setAge(age);
                member.setPh(ph);
                ref.push().setValue(member);
                Toast.makeText(MainActivity2.this,"Data insert successfully",Toast.LENGTH_LONG).show();

            }
        });

    }
    private void anhxa() {
        tongthu=findViewById(R.id.txttongthufr);
        tongchi=findViewById(R.id.txttongchifr);
        tongdu=findViewById(R.id.txttongdufr);
        btntext=findViewById(R.id.btntext);
    }
}
