package com.example.bookmanager1.TheLoaiSach;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.bookmanager1.DAO.ThemTheLoaiDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.Theloai;
import com.google.android.material.textfield.TextInputLayout;

public class ThemTheLoaiSachFragment extends Fragment {
    EditText matl,tentl,vitritl,motatl;
    Button btnthem,btnhuy,btnshow;
    Theloai nd;
    ThemTheLoaiDao dao;
TextInputLayout inputma,inputten,inputvitri,inputmota;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_themtheloai,container,false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        matl=view.findViewById(R.id.edmatheloai);
        tentl=view.findViewById(R.id.edtentheloai);
        vitritl=view.findViewById(R.id.edvitritheloai);
        motatl=view.findViewById(R.id.edmotatheloai);

        btnthem=view.findViewById(R.id.btnthemtheloai);
        btnhuy=view.findViewById(R.id.btnhuytheloai);
        btnshow=view.findViewById(R.id.btnshowtheloai);
        dao = new ThemTheLoaiDao(getContext());
//error
        inputma=view.findViewById(R.id.inputmatheloai);
        inputten=view.findViewById(R.id.inputtentheloai);
        inputvitri=view.findViewById(R.id.inputvitritheloai);
        inputmota=view.findViewById(R.id.inputmotatheloai);
        btnthem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String matlmoai = matl.getText().toString();
                String tentloai = tentl.getText().toString();
                String vitritloai = vitritl.getText().toString();
                String motatloai = motatl.getText().toString();

                if (matl.getText().toString().isEmpty()){
                    inputma.setError("ma tl khong duoc de trong");
                    return ;
                }
                if (tentl.getText().toString().isEmpty()){
                    inputten.setError("ten tl khong duoc de trong");
                    return ;
                }
                if (vitritl.getText().toString().isEmpty()){
                    inputvitri.setError("vi tri khong duoc de trong");
                    return ;
                }
                if (motatl.getText().toString().isEmpty()){
                    inputmota.setError("mota khong duoc de trong");
                    return ;
                }
                nd = new Theloai(matlmoai,tentloai,vitritloai,motatloai);
                dao.insert(nd);

            }
        });

    }
}
