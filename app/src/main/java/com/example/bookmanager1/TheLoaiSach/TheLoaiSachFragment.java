package com.example.bookmanager1.TheLoaiSach;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.bookmanager1.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class TheLoaiSachFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_theloaisach,container,false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FragmentTransaction transaction=getFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container_theloaisach,new ThemTheLoaiSachFragment());
        transaction.commit();
        BottomNavigationView bottomnav=view.findViewById(R.id.bottom_theloaisach);
        bottomnav.setOnNavigationItemSelectedListener(Navlister);
    }
    BottomNavigationView.OnNavigationItemSelectedListener Navlister=new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment=null;
            switch (menuItem.getItemId()){
                case R.id.nav_themtheloaisach:
                    selectedFragment=new ThemTheLoaiSachFragment();
                    break;
                case R.id.nav_showtheloaisach:
                    selectedFragment=new ShowTheLoaiSachFragment();
                    break;
            }
            FragmentTransaction transaction=getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container_theloaisach,selectedFragment);
            transaction.commit();
            return true;
        }
    };
}
