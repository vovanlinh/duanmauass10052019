package com.example.bookmanager1.TheLoaiSach;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bookmanager1.Adapter.TheLoaiAdaptercv;
import com.example.bookmanager1.DAO.ThemTheLoaiDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.Theloai;

import java.util.ArrayList;

public class ShowTheLoaiSachFragment extends Fragment {

    RecyclerView lv;
    ArrayList<Theloai> list;
    public static TheLoaiAdaptercv adapter;
    static ThemTheLoaiDao dao;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_showtheloaisach,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lv = view.findViewById(R.id.lv_showtheloaisach);
        list = new ArrayList<>();
        dao = new ThemTheLoaiDao(getContext());
        list =(ArrayList<Theloai>) dao.getTheLoai();
        LinearLayoutManager mlinerlayoutmanager=new LinearLayoutManager(getContext());
        lv.setLayoutManager(mlinerlayoutmanager);
        adapter = new TheLoaiAdaptercv(getContext(), list);
        lv.setAdapter(adapter);

    }
    public static void xoaTheloai(Theloai s){

        dao.delete(s);
        capnhatLV();

    }

    public static void capnhatLV(){

        adapter.notifyDataSetChanged();
    }
}
