package com.example.bookmanager1.TheLoaiSach;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.bookmanager1.DAO.ThemTheLoaiDao;
import com.example.bookmanager1.R;
import com.example.bookmanager1.model.Theloai;

public class SuaTheLoaiSach extends AppCompatActivity {
    Theloai sach;
    ThemTheLoaiDao dao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sua_the_loai_sach);
        // lay data tu bundle
        final EditText matl = findViewById(R.id.edmatheloaisua);
        final EditText tentl = findViewById(R.id.edtentheloaisua);
        final EditText vitritl = findViewById(R.id.edvitritheloaisua);
        final EditText motatl = findViewById(R.id.edmotatheloaisua);
        Button btnsua = findViewById(R.id.btnsuatl);
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("suaTheloai");

        if (bundle != null) {
            String matheloai = bundle.getString("matheloai", "");
            String tentheloai = bundle.getString("tentheloai", "");
            String vitritheloai = bundle.getString("vitritheloai", "");
            String motatheloai = bundle.getString("motatheloai", "");
            //dua data len EditText
            matl.setText(matheloai);
            tentl.setText(tentheloai);
            vitritl.setText(vitritheloai);
            motatl.setText(motatheloai);

            // khoa EditText MaSach

            if (!matheloai.isEmpty()) {
                matl.setEnabled(false);
            }
        }
        dao = new ThemTheLoaiDao(this);
btnsua.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        String clma = matl.getText().toString();
        String clten = tentl.getText().toString();
        String clvitri = vitritl.getText().toString();
        String clmota = motatl.getText().toString();
        Theloai sach = new Theloai(clma,clten,clvitri,clmota);

        dao.update(sach);
        finish();
    }
});

    }
    @Override
    protected void onResume() {
        super.onResume();
    }
}