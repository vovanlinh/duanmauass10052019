package com.example.bookmanager1.DAO;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.bookmanager1.Adapter.NguoiDungAdaptercv;
import com.example.bookmanager1.HoaDon.ThemHoaDonChiTiet;
import com.example.bookmanager1.Sach.ShowSachFragment;
import com.example.bookmanager1.model.NDUI;
import com.example.bookmanager1.model.Sach;
import com.example.bookmanager1.model.Theloai;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ThemSachDao {
    public static Context context;
    private DatabaseReference mReferencend;
    NDUI nonUI;
   // Context context;
    String nguoidungId;
    public NguoiDungAdaptercv adapter;
public ThemSachDao(Context context){
this.mReferencend=FirebaseDatabase.getInstance().getReference("Sach");
this.context=context;
this.nonUI=new NDUI(context);
}
    public List<Sach>getAllSach() {

        final List<Sach> list = new ArrayList<Sach>();

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Sach object and use the values to update the UI
                list.clear();
               for (DataSnapshot data:dataSnapshot.getChildren()){
                    Sach s = data.getValue(Sach.class);
                    list.add(s);
//
                }


                ShowSachFragment.capnhatLV();
            //    ThemHoaDonChiTiet.capnhatLVhdct();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mReferencend.addValueEventListener(listener);
        return list;
    }
    public List<Sach>getAllSachtest() {

        final List<Sach> list = new ArrayList<Sach>();

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Sach object and use the values to update the UI
                list.clear();
                for (DataSnapshot data:dataSnapshot.getChildren()){
                    Sach s = data.getValue(Sach.class);
                    list.add(s);
//
                }


              //  ShowSachFragment.capnhatLV();
                   ThemHoaDonChiTiet.capnhatLVhdct();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mReferencend.addValueEventListener(listener);
        return list;
    }

    public ArrayList<Sach> getIdbook(String idbook){
        final  ArrayList<Sach> arrayList=new ArrayList<Sach>();
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Sach object and use the values to update the UI
                arrayList.clear();

                for (DataSnapshot data:dataSnapshot.getChildren()){
                    Sach s = data.getValue(Sach.class);
                    arrayList.add(s);
//
                }


                  ShowSachFragment.capnhatLV();
                //ThemHoaDonChiTiet.capnhatLVhdct();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
       // mReferencend.addValueEventListener(listener);
        Query query=FirebaseDatabase.getInstance().getReference("Sach").orderByChild("maTL").equalTo(idbook);
        query.addListenerForSingleValueEvent(listener);
        return arrayList;
    }






    public ArrayList<String> LaySoLuongSachVaTien(){
final ArrayList<String> danhsachmasach=new ArrayList<>();
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Sach object and use the values to update the UI
                danhsachmasach.clear();
                String sd=null;
                for (DataSnapshot data:dataSnapshot.getChildren()){
                    Sach s = data.getValue(Sach.class);
                    sd=s.getMaSach();
                    sd+="-"+s.getGiabia();
                    sd+="-"+s.getSoluong();
                    danhsachmasach.add(sd);
//
                }


              //  ShowSachFragment.capnhatLV();
                ThemHoaDonChiTiet.capnhatLVTL();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mReferencend.addValueEventListener(listener);

return danhsachmasach;
    }

    public void insert(Sach s) {


        nguoidungId = mReferencend.push().getKey();

        mReferencend.child(nguoidungId).setValue(s).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
               nonUI.toast("insert Thanh cong");
                Log.d("insert","insert Thanh cong");


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                nonUI.toast("insert That bai");
                Log.d("insert","insert That bai");
            }
        });


    }

    public void update(final Sach s) {
        mReferencend.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot data : dataSnapshot.getChildren()) {

                    if (data.child("maSach").getValue(String.class).equals(s.maSach)){
                        nguoidungId = data.getKey();
                        Log.d("getKey", "onCreate: key :" + nguoidungId);


                        mReferencend.child(nguoidungId).setValue(s)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                       nonUI.toast("update Thanh cong");
                                        Log.d("update","update Thanh cong");


                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        nonUI.toast("update That bai");
                                        Log.d("update","update That bai");
                                    }
                                });

                    }

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void delete(final Sach s) {

        mReferencend.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot data : dataSnapshot.getChildren()) {

                    if (data.child("maSach").getValue(String.class).equalsIgnoreCase(s.maSach)){
                        nguoidungId = data.getKey();
                        Log.d("getKey", "onCreate: key :" + nguoidungId);


                        mReferencend.child(nguoidungId).setValue(null)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                      nonUI.toast("delete Thanh cong");
                                        Log.d("delete","delete Thanh cong");


                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        nonUI.toast("delete That bai");
                                        Log.d("delete","delete That bai");
                                    }
                                });

                    }

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }



}
