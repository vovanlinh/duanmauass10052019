package com.example.bookmanager1.DAO;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.bookmanager1.Adapter.NguoiDungAdaptercv;
import com.example.bookmanager1.NguoiDung.ShowNguoiDungFragment;
import com.example.bookmanager1.model.NDUI;
import com.example.bookmanager1.model.Nguoidung;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ThemNguoiDungDao {
    private DatabaseReference mReferencend;
    NDUI nonUI;
    Context context;
    String nguoidungId;
    public NguoiDungAdaptercv adapter;
public ThemNguoiDungDao(Context context){
this.mReferencend=FirebaseDatabase.getInstance().getReference("Nguoidung");
this.context=context;
this.nonUI=new NDUI(context);
}
    public List<Nguoidung>getNguoiDung() {

        final List<Nguoidung> list = new ArrayList<Nguoidung>();

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Sach object and use the values to update the UI
                list.clear();
               for (DataSnapshot data:dataSnapshot.getChildren()){
                    Nguoidung s = data.getValue(Nguoidung.class);
                    list.add(s);
//
                }
//                list.notify();

               ShowNguoiDungFragment.capnhatLV();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mReferencend.addValueEventListener(listener);
        return list;
    }
    public List<Nguoidung>getUser() {

        final List<Nguoidung> list = new ArrayList<Nguoidung>();

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Sach object and use the values to update the UI
                list.clear();
                for (DataSnapshot data:dataSnapshot.getChildren()){
                    Nguoidung s = data.getValue(Nguoidung.class);
                    list.add(s);
//
                }




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mReferencend.addValueEventListener(listener);
        return list;
    }



    public void insert(Nguoidung s) {


        nguoidungId = mReferencend.push().getKey();

        mReferencend.child(nguoidungId).setValue(s).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
             nonUI.toast("insert Thanh cong");
               Log.d("insert","insert Thanh cong");


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
          nonUI.toast("insert That bai");
                Log.d("insert","insert That bai");
            }
        });


    }

    public void update(final Nguoidung s) {
        mReferencend.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot data : dataSnapshot.getChildren()) {

                    if (data.child("usernd").getValue(String.class).equals(s.usernd)){
                        nguoidungId = data.getKey();
                       Log.d("getKey", "onCreate: key :" + nguoidungId);


                        mReferencend.child(nguoidungId).setValue(s)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                      nonUI.toast("update Thanh cong");
                                       Log.d("update","update Thanh cong");


                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                      nonUI.toast("update That bai");
                                        Log.d("update","update That bai");
                                    }
                                });

                    }

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



private boolean updateUser(String usernd, String passnd, String sdtnd, String hotennd) {
//getting the specified User reference
    DatabaseReference UpdateReference = FirebaseDatabase.getInstance().getReference("Nguoidung").child(usernd);

    Nguoidung User = new Nguoidung(usernd, passnd, sdtnd, hotennd);
//update User to firebase
    UpdateReference.setValue(User);
    Toast.makeText(context, "User Updated", Toast.LENGTH_LONG).show();
    return true;
}


    public ArrayList<Nguoidung> getIdbook(String user){
        final  ArrayList<Nguoidung> arrayList=new ArrayList<Nguoidung>();
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Sach object and use the values to update the UI
                arrayList.clear();

                for (DataSnapshot data:dataSnapshot.getChildren()){
                    Nguoidung s = data.getValue(Nguoidung.class);
                    arrayList.add(s);
//
                }




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        // mReferencend.addValueEventListener(listener);
        Query query=FirebaseDatabase.getInstance().getReference("Nguoidung").orderByChild("usernd").equalTo(user);
        query.addListenerForSingleValueEvent(listener);
        return arrayList;
    }



    public void delete(final Nguoidung s) {

        mReferencend.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot data : dataSnapshot.getChildren()) {

                    if (data.child("usernd").getValue(String.class).equalsIgnoreCase(s.usernd)){
                        nguoidungId = data.getKey();
                        Log.d("getKey", "onCreate: key :" + nguoidungId);


                        mReferencend.child(nguoidungId).setValue(null)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                       nonUI.toast("delete Thanh cong");
                                        Log.d("delete","delete Thanh cong");


                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                     //   nonUI.toast("delete That bai");
                                       // Log.d("delete","delete That bai");
                                    }
                                });

                    }

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }



}
