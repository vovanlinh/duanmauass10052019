package com.example.bookmanager1.DAO;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.bookmanager1.Adapter.NguoiDungAdaptercv;
import com.example.bookmanager1.HoaDon.ShowHoaDonFragment;
import com.example.bookmanager1.NguoiDung.ShowNguoiDungFragment;
import com.example.bookmanager1.model.HoaDon;
import com.example.bookmanager1.model.NDUI;
import com.example.bookmanager1.model.Nguoidung;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HoaDonDao {
    private DatabaseReference mReferencend;
    NDUI nonUI;
    Context context;
    String hoadonId;
public HoaDonDao(Context context){
this.mReferencend=FirebaseDatabase.getInstance().getReference("HoaDon");
this.context=context;
this.nonUI=new NDUI(context);
}
    public List<HoaDon>getHoaDon() {

        final List<HoaDon> list = new ArrayList<HoaDon>();

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Sach object and use the values to update the UI
                list.clear();
               for (DataSnapshot data:dataSnapshot.getChildren()){
                    HoaDon s = data.getValue(HoaDon.class);
                    list.add(s);
//
                }
//                list.notify();

               ShowHoaDonFragment.capnhatLV();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mReferencend.addValueEventListener(listener);
        return list;
    }

    public void insert(HoaDon s) {


        hoadonId = mReferencend.push().getKey();

        mReferencend.child(hoadonId).setValue(s).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                nonUI.toast("insert Thanh cong");
                Log.d("insert","insert Thanh cong");


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                nonUI.toast("insert That bai");
                Log.d("insert","insert That bai");
            }
        });


    }

    public void update(final HoaDon s) {
        mReferencend.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot data : dataSnapshot.getChildren()) {

                    if (data.child("maHoaDon").getValue(String.class).equals(s.maHoaDon)){
                        hoadonId = data.getKey();
                        Log.d("getKey", "onCreate: key :" + hoadonId);


                        mReferencend.child(hoadonId).setValue(s)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        nonUI.toast("update Thanh cong");
                                        Log.d("update","update Thanh cong");


                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        nonUI.toast("update That bai");
                                        Log.d("update","update That bai");
                                    }
                                });

                    }

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void delete(final HoaDon s) {

        mReferencend.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot data : dataSnapshot.getChildren()) {

                    if (data.child("maHoaDon").getValue(String.class).equalsIgnoreCase(s.maHoaDon)){
                        hoadonId = data.getKey();
                        Log.d("getKey", "onCreate: key :" + hoadonId);


                        mReferencend.child(hoadonId).setValue(null)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        nonUI.toast("delete Thanh cong");
                                        Log.d("delete","delete Thanh cong");


                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                       nonUI.toast("delete That bai");
                                        Log.d("delete","delete That bai");
                                    }
                                });

                    }

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }



}
