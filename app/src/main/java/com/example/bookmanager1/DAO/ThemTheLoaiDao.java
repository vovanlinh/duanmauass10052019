package com.example.bookmanager1.DAO;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.bookmanager1.Sach.ShowSachFragment;
import com.example.bookmanager1.Sach.SuaSach;
import com.example.bookmanager1.Sach.ThemSachFragment;
import com.example.bookmanager1.TheLoaiSach.ShowTheLoaiSachFragment;
import com.example.bookmanager1.model.NDUI;
import com.example.bookmanager1.model.Nguoidung;
import com.example.bookmanager1.model.Sach;
import com.example.bookmanager1.model.Theloai;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ThemTheLoaiDao {
    private DatabaseReference mReferencend;
    NDUI nonUI;
    Context context;
    String theloaiId;

public ThemTheLoaiDao(Context context){
this.mReferencend=FirebaseDatabase.getInstance().getReference("Theloai");
this.context=context;
this.nonUI=new NDUI(context);
}
    public List<Theloai>getTheLoai() {

        final List<Theloai> list = new ArrayList<Theloai>();

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Sach object and use the values to update the UI
                list.clear();
                for (DataSnapshot data:dataSnapshot.getChildren()){
                    Theloai s = data.getValue(Theloai.class);
                    list.add(s);
//
                }
                ShowTheLoaiSachFragment.capnhatLV();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mReferencend.addValueEventListener(listener);
        return list;
    }
    public List<Theloai>getTenTheLoai() {

        final List<Theloai> list = new ArrayList<Theloai>();

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Sach object and use the values to update the UI
                list.clear();
                for (DataSnapshot data:dataSnapshot.getChildren()){
                    Theloai s = data.getValue(Theloai.class);

                    list.add(s);
//
                }
                ThemSachFragment.capnhatLV();
//                ((SuaSach)context).capnhatLVsua();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mReferencend.addValueEventListener(listener);
        return list;
    }


public ArrayList<String>getidbook(){
    final ArrayList<String> arrayList=new ArrayList<>();
    ValueEventListener listener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            // Get Sach object and use the values to update the UI
            String s=null;
            arrayList.clear();
            for (DataSnapshot data:dataSnapshot.getChildren()){
                Theloai ds = data.getValue(Theloai.class);
                // s.getMaTL();
                // s.getTenTL();
                s=ds.getMaTL();
                s+="-"+ds.getTenTL();
                arrayList.add(s);
//
            }
            //    ThemSachFragment.capnhatLV();
            ShowSachFragment.capnhatLVTL();
//            ((SuaSach)context).capnhatLVsua();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    mReferencend.addValueEventListener(listener);
    return arrayList;
}


    public List<Theloai>getTenTheLoaiSua() {

        final List<Theloai> list = new ArrayList<Theloai>();

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Sach object and use the values to update the UI
                list.clear();
                for (DataSnapshot data:dataSnapshot.getChildren()){
                    Theloai s = data.getValue(Theloai.class);
                    // s.getMaTL();
                    // s.getTenTL();
                    list.add(s);
//
                }
            //    ThemSachFragment.capnhatLV();
    ((SuaSach)context).capnhatLVsua();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mReferencend.addValueEventListener(listener);
        return list;
    }
    public void insert(Theloai s) {


        theloaiId = mReferencend.push().getKey();

        mReferencend.child(theloaiId).setValue(s).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
               nonUI.toast("insert Thanh cong");
                Log.d("insert","insert Thanh cong");


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
               nonUI.toast("insert That bai");
                Log.d("insert","insert That bai");
            }
        });


    }

    public void update(final Theloai s) {
        mReferencend.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot data : dataSnapshot.getChildren()) {

                    if (data.child("maTL").getValue(String.class).equals(s.maTL)){
                        theloaiId = data.getKey();
                        Log.d("getKey", "onCreate: key :" + theloaiId);


                        mReferencend.child(theloaiId).setValue(s)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                       nonUI.toast("update Thanh cong");
                                        Log.d("update","update Thanh cong");


                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        nonUI.toast("update That bai");
                                        Log.d("update","update That bai");
                                    }
                                });

                    }

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void delete(final Theloai s) {

        mReferencend.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot data : dataSnapshot.getChildren()) {

                    if (data.child("maTL").getValue(String.class).equalsIgnoreCase(s.maTL)){
                       theloaiId = data.getKey();
                        Log.d("getKey", "onCreate: key :" + theloaiId);


                        mReferencend.child(theloaiId).setValue(null)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        nonUI.toast("delete Thanh cong");
                                        Log.d("delete","delete Thanh cong");


                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        nonUI.toast("delete That bai");
                                        Log.d("delete","delete That bai");
                                    }
                                });

                    }

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }



}
