package com.example.bookmanager1.model;

public class Theloai {
   public String maTL,tenTL,vitriTL,motaTL;

    public String getMaTL() {
        return maTL;
    }

    public void setMaTL(String maTL) {
        this.maTL = maTL;
    }

    public String getTenTL() {
        return tenTL;
    }

    public void setTenTL(String tenTL) {
        this.tenTL = tenTL;
    }

    public String getVitriTL() {
        return vitriTL;
    }

    public void setVitriTL(String vitriTL) {
        this.vitriTL = vitriTL;
    }

    public String getMotaTL() {
        return motaTL;
    }

    public void setMotaTL(String motaTL) {
        this.motaTL = motaTL;
    }

    public Theloai(String maTL, String tenTL, String vitriTL, String motaTL) {
        this.maTL = maTL;
        this.tenTL = tenTL;
        this.vitriTL = vitriTL;
        this.motaTL = motaTL;
    }

    public Theloai() {
    }
}
