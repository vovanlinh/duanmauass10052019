package com.example.bookmanager1.model;

import java.util.ArrayList;

public class HoaDonChiTiet {
 public    String maHDCT;
  public   String maHoaDon;
  public   String maSach;
  public   String soLuongMua;
  public String giaSach;

    public HoaDonChiTiet() {
    }

    public String getMaHDCT() {
        return maHDCT;
    }

    public void setMaHDCT(String maHDCT) {
        this.maHDCT = maHDCT;
    }

    public String getMaHoaDon() {
        return maHoaDon;
    }

    public void setMaHoaDon(String maHoaDon) {
        this.maHoaDon = maHoaDon;
    }

    public String getMaSach() {
        return maSach;
    }

    public void setMaSach(String maSach) {
        this.maSach = maSach;
    }

    public String getSoLuongMua() {
        return soLuongMua;
    }

    public void setSoLuongMua(String soLuongMua) {
        this.soLuongMua = soLuongMua;
    }

    public String getGiaSach() {
        return giaSach;
    }

    public void setGiaSach(String giaSach) {
        this.giaSach = giaSach;
    }

    public HoaDonChiTiet(String maHoaDon,String maHDCT, String maSach, String soLuongMua, String giaSach) {
        this.maHDCT = maHDCT;
        this.maHoaDon = maHoaDon;
        this.maSach = maSach;
        this.soLuongMua = soLuongMua;
        this.giaSach = giaSach;
    }
}
