package com.example.bookmanager1.model;
import com.google.firebase.database.IgnoreExtraProperties;
public class Nguoidung {
   public String usernd,passnd,sdtnd,hotennd;


    public Nguoidung(String usernd, String passnd, String sdtnd, String hotennd) {
        this.usernd = usernd;
        this.passnd = passnd;
        this.sdtnd = sdtnd;
        this.hotennd = hotennd;
    }

    public Nguoidung() {
    }

    public String getUsernd() {
        return usernd;
    }

    public void setUsernd(String usernd) {
        this.usernd = usernd;
    }

    public String getPassnd() {
        return passnd;
    }

    public void setPassnd(String passnd) {
        this.passnd = passnd;
    }

    public String getSdtnd() {
        return sdtnd;
    }

    public void setSdtnd(String sdtnd) {
        this.sdtnd = sdtnd;
    }

    public String getHotennd() {
        return hotennd;
    }

    public void setHotennd(String hotennd) {
        this.hotennd = hotennd;
    }
}
